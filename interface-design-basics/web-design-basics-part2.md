## 7. The process of a good design

Now that you know all the basic information any designer should know, it is time for you to learn the process that is behind designing a good interface. This section is devoted to the design cycle and the approaches that you can use when developing an interface. Once you finish reading it, you should have clear idea about what exactly you need to do in order to make sure your clients like your work !

### 7.1 Basic concepts and the design cycle

The process of creating a good design is divided into four specific stages, that go from the abstract where you can generate new ideas to the concrete where you build something for your client to see:

1. Sketching
2. Wireframing
3. Mocking
4. Prototyping

These four stages are all different, and in each of them you will attend specific problems of your design. The path of a designer can therefore be summarized by the following image:

![design_process](http://uxmovement.com/wp-content/uploads/2012/08/sketch-before-wireframe.png)

The last step of the image, **code** refers to creating a prototype. There are actually three main forms of prototyping, however before going into that, you will learn the main differences between all the four stages, and what you are expected to do in each stage as well. The next sections then will teach you the basics of the design process,but for now it should be clear that a good designer always goes through all the stages when creating a design for a website that is intended to be used by people. 

#### 7.1.1 Sketches

Sketches are useful when you want to communicate an recently created idea and they also are an important part in the aid of thought. If an idea pops into your head and you want to quickly jot it down so that you do not forget or if you just want a visual representation on how a new functionality could be implemented, then sketches are the way to go. 

As [uxmatters](http://www.uxmatters.com/mt/archives/2010/05/sketches-and-wireframes-and-prototypes-oh-my-creating-your-own-magical-wizard-experience.php) defines, sketches are:

> "(...) sketching is rapid, freehand drawing that we do with no intention of its becoming a finished product (...)".

![sketch][2]

Sketches usually are:

- Quickly/timely (fast to create)
- Inexpensive 
- Plentiful and disposable
- Clear vocabulary (help you organize and present ideas to people)
- Minimal detail
- Appropriate level of detail (they can be as complex as you want)
- They suggest and explore new ideas and paths instead of solidifying and confirming them
- They are ambiguous to interpretation, which leaves holes that will be understood and filled with different ideas by different people, thus contributing to an even greater generation of ideas

Thanks to this characteristics sketches are the first step of any design process, and as a designer, you will move forward from this point.

It is important for you to understand that Sketches are not a final product of an idea, they are a process of refinement and generation of ideas. As *Suwa and Tversky, 2002* said:

> "(...) designers do not draw sketches to externally represent ideas that are already consolidated in their minds. Rather, they draw sketches to try out ideas, usually vague and uncertain ones. By examining the externalizations, designers can spot problems they may not have anticipated. More than that, they can see new features and relations among elements that they have drawn, ones not intended in the original sketch. These unintended discoveries promote new ideas and refine current ones (...)" 

#### 7.1.2 Wireframes

The wireframe is the next step of a designer. After exploring ideas with the sketch, the designer then makes a wireframe. Wireframes are more specific and and concrete than sketches, they usually represent a page layout with the ideas that you have chosen to perfect in the sketching phase. 

According to the Wikipedia page on [wireframes](http://en.wikipedia.org/wiki/Website_wireframe):

> "The wireframe depicts the page layout or arrangement of the website’s content, including interface elements and navigational systems, and how they work together.[2] The wireframe usually lacks typographic style, color, or graphics, since the main focus lies in functionality, behavior, and priority of content.[3] In other words, it focuses on what a screen does, not what it looks like.[4]"

![wire][3]

Just like sketches, wireframes have attributes on their own:

- Quick (although still fast to create, they do take longer than sketches)
- Inexpensive
- Viable (wireframes depict the selected ideas that made it through the cut, thus they are less plentiful and are more likely to become what you are going to end up designing)
- Clear vocabulary (use symbols and expressions easily understood by every one involved)
- Minimally detailed (they have more detail than sketches though)
- Confirmatory (wireframes represent concepts that need validation, but do not yet represent concrete decision, they can change at any time)
- They are still ambiguous to interpretation

As you can see, Wireframes only differ a little bit from sketches. Unfortunately, for this reason, many professionals in the web industry actually skip sketching and they jump right into wireframing. This is a terrible mistake and [uxmovement](http://www.uxmatters.com/mt/archives/2010/05/sketches-and-wireframes-and-prototypes-oh-my-creating-your-own-magical-wizard-experience.php) explains that sketching before wireframing has many advantages such as:

- Sketching is a process of generating ideas, and thus it will help you make better and more original products.
- Wireframing without previous sketching is harder because you are generating ideas and at the same time trying to pixelize them into an interface. Therefore, sketching before will make wireframing faster.
- Because sketching generates and organizes ideas it will also result in more detailed and refined wireframes. 
- Sketching is all about concepts and ideas, not about art. You can be a terrible artist and make a very good and functional.

Last, but not least, reading, evaluating and designing wireframes is also very important, specially when you have to show them to a potential client. For this effect, [fuzzymath's blog](blog.fuzzymath.com) created three posters that will teach you how to [read](http://blog.fuzzymath.com/2011/07/12/how-to-read-a-wireframe/), [evaluate](http://blog.fuzzymath.com/2011/07/19/how-to-evaluate-a-wireframe/) and [design](http://blog.fuzzymath.com/2011/07/26/how-to-design-a-wireframe/) wireframes. Unfortunately the posters are too big to post here, but you should totally check them out by clicking the links. For those of you interested, they even have a [french version](http://blog.fuzzymath.com/2011/08/19/wireframe-basics-now-in-french/) of the posters.

#### 7.1.3 Mockups

The mockup is the next step after the wireframe. A mockup is a model or a design of the site or app you plan to make, a screenshot of a website.  Mockups are colorful, have images, different types of fonts and are close to the final design of the website.

![mock][4] 

[Have you confused Wireframes with Mockups?](http://creately.com/diagram-type/article/have-you-confused-wireframes-mockups) defines mockups in contrast to wireframes. To quote the artcile: 

> "(...) If wireframe is the skeleton, then mockup is the skin!(...)". 

As we progress in the design spiral, more and more details are uncovered. Therefore, even though mockups share some similarities with wireframes, they are mainly distinguished by their differences:

- Not quick (the design has is being thought and considered and this is a work of detail)
- Closer to definitive (probably the final design)
- Considerable level of detail
- Not ambiguous

A mockup has all the graphic elements and graphic polish that the wireframe does not have. For this reason, some designers use photo and image editing tools to create mockups, such as [Adobe Photoshop](http://www.adobe.com/products/photoshopfamily.html) or [Adobe Fireworks](http://www.adobe.com/products/fireworks.html). There are however, many specialized mockup programs that you can use. These however will not allow you to make interfaces as detailed as the previous products because they are usually more concerned with functionality than with the details of the design itself - that is usually up to the artist designing the website.

#### 7.1.4 Prototypes

The prototype is the final step of the iteration as a designer. Prototypes differ from mockups in that mockups are like static screenshots of the website and only contain static information, and prototypes allow real interaction with the user. A prototype is a coded webpage that tries to implement and demonstrate the design concepts you have selected in the previous stages, so the client can see your work in action. A prototype is not meant to be the final design nor the final version of the website, but merely something you can show to your client and something he can experiment on. To quote [howtomakemillionswithyournewidea](http://www.howtomakemillionswithyournewidea.com/mock-up-prototype-difference/2012/08/06), a prototype is:


> "A prototype actually includes the functionality of your invention, at least in part. Not only can you test the design features of your invention with a prototype, but you can also showcase the functional features of the invention and fix any issues before you get too far into the manufacturing process."

![proto][5]

In sum, prototypes should allow the user to:

- experience content and interactions with the interface.
- test the main interactions in a way similar to the final product.
- have a close image of the stage of the project and of what it is going to look like in the end. 

#### 7.1.5 Additional information

As usual, we have only scratched the surface of each stage in an effort to make this article *small*. Some of you may yet be confused by some of the stages, and others may wish learn more. For these reasons we provide the following links and files as additional information that we encourage you to check:

- [Sketches and Wireframes and Prototypes! Oh My! Creating Your Own Magical Wizard Experience](http://www.uxmatters.com/mt/archives/2010/05/sketches-and-wireframes-and-prototypes-oh-my-creating-your-own-magical-wizard-experience.php)
- [Why It’s Important to Sketch Before You Wireframe - uxmovement](http://uxmovement.com/wireframes/why-its-important-to-sketch-before-you-wireframe/)
- [Sketches/Mockups Vs WireFrame vs Prototype - wireframemockups](http://www.wireframemockups.com/sketchesmockups-vs-wireframe-vs-prototype)
- [Wireframing, Prototyping, Mockuping – What’s the Difference? - designmodo](http://designmodo.com/wireframing-prototyping-mockuping/)
- [What is the difference betweetn a mockup and a protoype? - wiki.answers](http://wiki.answers.com/Q/What_is_the_difference_between_a_mock_up_and_a_prototype?#slide=1)
- [What’s the Difference Between a Wireframe, Mock-Up, and Prototype? - bridging-the-gap](http://www.bridging-the-gap.com/wireframe-mock-up-prorotype-difference/)
- [WIREFRAMING, PROTOTYPING, MOCKUPING - slideshare](http://www.slideshare.net/mtreder/wireframing-prototyping-mockuping)
- [Have you confused Wireframes with Mockups? - creately](http://creately.com/diagram-type/article/have-you-confused-wireframes-mockups)
- [What Sketches (and Prototypes) Are and Are Not- cs.cmu](http://www.cs.cmu.edu/~bam/uicourse/Buxton-SketchesPrototypes.pdf)
- [Wireframe vs mockup, why and when - slideshare](http://www.slideshare.net/voutulny/wireframe-vs-mockup-why-and-when)
 

### 7.2 Approaches to the design cycle

Now that you know all of the basic concepts and how they all fit together it is time to present you to the prototyping cycle itself. There are three main processes by which you can develop follow the design cycle and reach the prototype stage:

- Throw-away 
- Incremental
- Evolutionary

Each approach can be used depending on what you are building. Therefore in the following sections we will make a brief visit to each of them.

#### 7.2.1 Throw-away approach

In this approach, the prototype is built and tested by the users. Once such is done, the knowledge gained is used the build the main application and the prototype is thrown away. Because the sole purpose of the prototype in this approach is to make sure that the client likes the product, you can build dummy prototypes. Dummy prototypes **should never be the final version of anything** but they are specially useful in this approach because you can use many strategies and hacks that will increase its building speed such as:

- placing less emphasis on the efficiency of the implementation
- accepting less reliable or poor quality code
- using simplified algorithms
- wizard of Oz approach
- using low-fidelity media
- using fake data and other content
- using paper instead of a running computer system

![throw-away-prototyping](https://dl.dropboxusercontent.com/u/785815/IPM/throw-away-prototyping.png)

Although this approach is used by some web developers, it is dangerous because when used by inexperienced designers, it usually means you only iterated through the design cycle one time, and thus your product may be lacking features that would have otherwise been seen by the users and testers in future and repeated iterations of the product.

On the other hand, if several iterations are made, this approach provides a clean sheet for a starting point, thus giving the designers more space to think about the ideas and lessons learned from the previous iterations.

For additional information on this approach you can visit the links:

- ['Throw-away' Prototyping - teach-ict](http://www.teach-ict.com/as_a2_ict_new/ocr/A2_G063/331_systems_cycle/prototyping_RAD/miniweb/pg4.htm)
- [Throw Away / Rapid Prototyping - sqa](http://www.sqa.org.uk/e-learning/IMAuthoring01CD/page_08.htm)

#### 7.2.2 Incremental approach

In this approach the final product is built as separate components, one by one. Each component is created and designed separately and so the final product ends up being a series of different products that complement each other. You can see this as the Microsoft Office Suit, which is composed of several different products such as Microsoft Word, Excel, and so on. 

![incremental-prototyping](http://rootsitservices.com/pictures/incremental_model1.jpg)

Although it may be viable for some web applications, it is usually not used in the creation of websites.

For additional information on this approach you can visit the links:

- [Incremental Prototyping - sqa](http://www.sqa.org.uk/e-learning/IMAuthoring01CD/page_09.htm)

#### 7.2.3 Evolutionary approach

In this version, the prototype is not discarded. Instead it serves as a basis for the next design iteration. Here the project seems to be evolving from a very limited initial release into the final product. This is the most common approach used to making website and web applications, but it does require good quality prototypes.

![evolutionary-prototyping](https://dl.dropboxusercontent.com/u/785815/IPM/evolutionary-prototyping.png)

For additional information on this approach you can visit the links:

- [Evolutionary Prototyping - teach-ict](http://www.teach-ict.com/as_a2_ict_new/ocr/A2_G063/331_systems_cycle/prototyping_RAD/miniweb/pg3.htm)
- [Evolutionary Prototyping - sqa](http://www.sqa.org.uk/e-learning/IMAuthoring01CD/page_10.htm)

#### 7.2.4 Additional information

As usual this extra section provides you with additional information on the prototyping approach that you can use. One thing to notice however, is that these prototyping strategies are based on many SDS's, so do not be surprised if some of the referenced material actually illustrates SDS strategies in order to explain a prototyping approach. 

- [Software prototyping - Wiki](http://en.wikipedia.org/wiki/Software_prototyping)
- [Software Prototyping - slideshare](http://www.slideshare.net/drjms/software-prototyping)
- [What are the differences between throwaway and evolutionary prototypes? - stackexchange](http://programmers.stackexchange.com/questions/109409/what-are-the-differences-between-throwaway-and-evolutionary-prototypes)
- [Prototyping models](http://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=9&ved=0CGUQFjAI&url=http%3A%2F%2Fcs.wcsu.edu%2Fjoelw%2Foosw%2FModels.ppt&ei=RXPUUu__A8zQ7AaR_ICoDw&usg=AFQjCNHrUJfwwb-W7y4O9m4KJLBr1B9Lxw&sig2=9JXIEG9i01n-HSZAE2VbAA&bvm=bv.59026428,d.ZGU&cad=rja)
- [Prototyping strategies](http://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=11&ved=0CH4QFjAK&url=http%3A%2F%2Fwww.cse.dmu.ac.uk%2F~jtb%2FHCIprototyping.ppt&ei=HmrUUvT2FuGt7Qbq7oHIDg&usg=AFQjCNGxlkfyk5Emyx6y2lmJcXDjPL8sUw&sig2=KQR-fD5JoMlGsObFGuuxGw&bvm=bv.59026428,d.ZGU&cad=rja)

### 7.3 Tools that you can use

To create a website design you can use anything from a notebook to computer software.

 - Microsoft word
Microsoft word for windows can actually be used for a lot, just check [this](http://www.youtube.com/watch?v=RZp7BvQJnU8) out.

 - [moqups](https://moqups.com/)
Moqups is a nifty HTML5 App used to create wireframes, mockups or UI concepts, prototypes
depending on how you like to call them.

 - [ninjamock](http://ninjamock.com/)
NinjaMock makes it incredibly easy and fast to develop the entire concept and test usability of our apps. 

 - [cacoo](https://cacoo.com/lang/en/?ref=logo)
Cacoo is a user friendly online drawing tool that allows you to create a variety of diagrams such as site map, flowchart, mind map, wire frame, UML diagram and network diagram. Cacoo can be used free of charge.

 - [framebox](http://framebox.org/)
 You can drag and drop, re-size and copy/paste the UI units, but its main focus is that it allows you to create your mockup/prototype very, this app is FREE and only requires a quick registration.

 - [Pencil Project](http://pencil.evolus.vn/Default.html)
Pencil is built for the purpose of providing a free and open-source GUI prototyping tool that people can easily install and use to create mockups in popular desktop platforms.

 - [mockflow](http://www.mockflow.com/)
MockFlow Design Cloud provides design tools and collaboration services for designers. Its flagship software - "MockFlow WireframePro" is used by thousands of customers worldwide. Free and Paid version.

 - [gliffy](http://www.gliffy.com/index-d.php)
Gliffy is an online diagramming service that helps users communicate with a combination of shapes, text, and lines.




### 7.4 Going further

In this chapter we only covered the design process and the prototyping approaches. The slides of the class however, have a lot more content. They start with a brief case study of a system made for the OMS (Organização Mundial de Saúde in portuguese, which translates to *World Health Organization: WHO*), followed by a quick lesson on the role of the design. Then the slides introduce the read to two stages of the design cycle (sketching and prototyping) and the slides finish with the evaluation and analysis of paper prototypes and dynamic interactions. The slides also mention other subjects such as computational prototyping and tools that you can use. To check out the all this information and more, feel free to hop in the slides and learn more!

- [IPM4-Sketching-Prototyping11-12-en-clip][IPM-4]


## 8. Design Rules

Every good interface ever made shares a set of common rules. Design rules are so important that some companies (like Aplle) even have their own books and sets of interface rules. Design rules are important and there are many standards that you can follow when designing an interface for a website or a web application, like the [ISO 9241](http://en.wikipedia.org/wiki/ISO_9241) set of rules. In this chapter however, we will focus on a more practical approach. First we will intrudoce you to a set of general interface rules that you must always try to follow, no matter what. Then, we will present you with some specific website design rules. In the end you should have a good idea of how to make a good and simple interface for your users, by following these rules.

### 8.1 General interface rules

In this section we present you the general rules of interface design that you should **always** use, no matter what kind of website or application that you are making. These are the foundation of every good design, and in dout you will always be better off using these rules than using none. 

#### 8.1.1 Shneiderman 8 Golden Rules

Shneiderman created 8 golden rules that apply to any interface design. These rules are a must know for any designer, specially because they are general enough that you can apply them to most situations. Here we present the 8 rules with a brief description of each one. For more information and details on how this information is used in real websites and examples, you can check the additional information.

1. Strive for consistency
    - Consistent sequences of actions should be required in similar situations, do not surprise users by solving simillar problems in different ways.
    - Identical terminology (same tone, colour pallete, dialog style, etc) should be used in prompts, menus, and help screens.
    - Consistent commands should be employed throughout (use standards or create them for your app).
2. Enable frequent users (or power users) to use shortcuts
    - Allow expert users to use keyboard shortcuts, abbreviations, function keys, hidden commands, and macro facilities. This way your app can have a GUI for newcomers and advanced settings for experts.
3. Offer informative feedback
    - For every user action there must be feedback. The system must always inform the user that it has received the input information or that it is processing it.  
4. Design dialogs to yeild closure
    - Any sequence of actions from a user should always be structured and organized in groups with a beggining, a middle and an end.
5. Offer error prevention and simple error handling
    - Design the system so the user cannot make any serious error.
    - If a serious error does occur however, the system should be able to detect it and offer an easy way out with minimized damage.
6. Permit easy reversal of actions
    - Allowing a easy reversal of actions revlieves user anxiety (errors can easily be undone) and promotes the exploration of unknown features.
7. Support internal locus of control
    - The users must feel that they are in control of the system and not otherwise.
8. Reduce short-term memory load
    - Humans have limited short-term memory
    - Displays should follow the [KISS](http://en.wikipedia.org/wiki/KISS_principle) terminology
    - Multiple page displays should be consolidated 
    - Window motion should be reduced

For more literature about this subject, visit:

- [Shneiderman's "Eight Golden Rules of Interface Design" - faculty.washington](http://faculty.washington.edu/jtenenbg/courses/360/f04/sessions/schneidermanGoldenRules.html)
- [8 Golden Rules of Interface Design - softwarequality-cognitivepsyc](http://softwarequality-cognitivepsyc.blogspot.pt/2010/10/8-golden-rules-of-interface-design.html)
- [The Eight Golden Rules of interface design - cs.umd](http://www.cs.umd.edu/~ben/goldenrules.html)
- [HCI: Golden Rules of Interface Design - cs.drexel](https://www.cs.drexel.edu/~introcs/Fa10/notes/04.3_HCI/goldenRules.html?CurrentSlide=5)
- [Design Principles - learnline.cdu](http://learnline.cdu.edu.au/units/hit381/resources/popups/8goldenrules.html)

#### 8.1.2 Norman's 7 principles

[Donald A. Norman](http://en.wikipedia.org/wiki/Donald_Norman) is a conceptuated academic in the are of design and usability engineering. His work applies not just to computer interfaces, but also to other areas such as ergonomics. In his book, [The Design of Everyday Things](http://en.wikipedia.org/wiki/The_Design_of_Everyday_Things) he stated seven principles that any designer should follow, no matter what:

1. Use both knowledge in the world and knowledge in the head.
    - Tasks are easier to learn and understand if they share simillarities with the surrounding world. 
2. Simplify the structure of tasks.
    - Less is More. Tasks should be simple, minimizing the amount of planning and problem solving required. 
3. Make things visible: bridge the gulfs of Execution and Evaluation.
    - When executing a task a user should know in advance what is going to happen, and when evaluating the feedback from the app, the user should also understand it in relation to the actions he took.
4. Get the mappings right.
    - This rule is mainly applied to the area of ergonomics and is out of the scope of this article, but the additional links we provide to cover it, so you are free to check them. Simply put, the designer should make sure that the user understands the relations between his actions, what is possible to do with the system, what is going to happen once the actions take place and how the system responds to user input.
5. Exploit the power of constraints, both natural and artificial.
    - Do not allow the user to be able to do several or unrelated actions in a single situation. Constraints are your friends, make sure the user is only allowed to make one action in any given situation. This way it will be easier for the user to focus on the current task and on the work flow he is currently engadged on.
6. Design for error.
    - Be prepared for errors, as they will eventualy happen.
7. When all else fails, standardize.
    - When your tasks are hard to explain in an intuitive manner, make sure you follow an universal standard that the user will recognize.

Another version that summarizes his principles can be found in the [Normans Principles - ccit333.wikispaces.com](http://ccit333.wikispaces.com/Normans+Principles) webpage, but it all boils to the same. If you were paying attention to everything said until now, you probably realize that some of these rules are also part of the Shneiderman 8 Golden Rules. In fact that is true, some rules are so important that you must never forget them. 

Since there is a lot of academic reserach in this area, and since these principles are known by all serious designers as well, I strongly recommend you to check at least the some of the links below. There are academic papers, powerpoint classes and websites that will make sure you completely understand this. Some of the links also provide real life examples, something that we currently cannot cover due the space constraints. 

- [Intuitive Interfaces: A literature review of the Natural Mapping principle and Stimulus Response compatibility - idemployee.id.tue.nl](http://www.idemployee.id.tue.nl/g.w.m.rauterberg/publications/NATMAPreport.pdf)
- [Don Norman & The Design of Everyday Things - katvision.com](http://katsvision.com/canm606/session_3/03_norman_01.pdf)
- [HCI Lecture 1: Principles - inf.ed.ac.uk](http://www.inf.ed.ac.uk/teaching/courses/hci/1011/lecs/1_principles.pdf)
- [Design Principles - slideshare.net](http://www.slideshare.net/gelvan/design-principles)
- [Summary of Don Norman's Design Principles - csun.edu](http://www.csun.edu/science/courses/671/bibliography/preece.html)
- [Norman’s 7 Principles - sites.google.com](https://sites.google.com/a/nu.edu.pk/hci-060129/lectures-1/norman-s-7-principles)
- [The Design of Everyday Things - sharritt.com](http://www.sharritt.com/CISHCIExam/norman.html)
- [Prescriptive Theories - cs.umd.edu](http://www.cs.umd.edu/class/fall2002/cmsc838s/tichi/prescriptive.html)

#### 8.1.3 Nielsen's 10 heuristics

Nielsen developed a set of 10 rules that should be used to evaluate interfaces. While the former sub sections introduced you to sets of rules used to design interfaces, these are used by professionals to evaluate how good or bad an interface really is. Even though many of these rules are a repetition of what has been previously said, you should nevertheless read through them and through the recommended additional information.

1. Visibility of system status
    - Keep users informed about system status.
    - The system must give feedback to the user of what is happening.
    - It should do so with a small delay as well, the response time is important.
2. Match between system and the real world
    - The system should speak the user's language and make usage of common language.
    - The system should not restrain user input (like having a maximum character's limit for a user's name).
    - The system can make use of metaphors that ease understanding (like the recycle bin in Windows, which means that the trash goes there).
3. User control and freedom 
    - Exits should be clear and marked.
    - Always have an "exit-panic-button" ready in case your user screws up.
    - Allow the users to undo operations.
4. Consistency and standards
    - Follow platform standards (if you are making a web app for a Windows 8 tablet, use Windows standards, if you are making a social website, use Facebook and Google+ standards).
    - Follow the [principle of least astonishment](http://en.wikipedia.org/wiki/Principle_of_least_astonishment).
5. Error prevention
    - Do not give users the oportunity to make errors.
    - Disable illegal operations.
    - Selection is less prone to errors than typing.
6. Recognition rather than recall
    - Minimize the user's memory load. The user should not have to remember steps and procedures, all the needed information should be visible.
7. Flexibility and efficiency of use
    - Have shortcuts for frequent operations.
    - Allow macros for complex or long sequences of operations.
8. Aesthetic and minimalist design
    - Simplicity.
    - Less is more.
    - Group simillar content.
    - Align controls, use a grid.
    - Choose labels carefully.
9. Help users recognize, diagnose, and recover from errors
    - Have precise error messages.
    - Tell the user how the error occurred and how it can be fixed.
    - Hide technical details until requested 
10. Help and documentation
    - In general users do not read documentation, unless they have no other choice.
    - But user guides and online help are essential.
    - User guides and online help should be searchable, concise, task-oriented and interactive if possible (like a video, for example).

Now that you know the basic rules used by experts to evaluate interfaces you are now ready to know if your interface is good or bad. If it does not obey all the previous rules, then you are doing something wrong. This said however, it is also important to keep context in mind. As said in the beggining of this chapter, this rules are used mainly for general interface development. If you are making a simple website, then creating help and documentation for it is probably not going to help the user navigate at all in your website. In fact, if your website requires documentation so the user is able to navigate it, you should probably just start from scratch again as you did a terrible and non-intuitive website.

Following is some additional information on Nielsen's 10 heuristics. We strongly recommend you to check the first couple of links, specially the youtube video that does a decent job explaining the rules with examples and the the slideshare presentation that does the same:

- [10 Usability Heuristics for User Interface Design - nngroup.com](http://www.nngroup.com/articles/ten-usability-heuristics/)
- [10 Usability Heuristics - youtube.com](http://www.youtube.com/watch?v=hWc0Fd2AS3s)
- [Ten Usability Heuristics with example - slideshare.net](http://www.slideshare.net/sacsprasath/ten-usability-heuristics-with-example)
- [Heuristic Evaluation - usabilityfirst.com](http://www.usabilityfirst.com/usability-methods/heuristic-evaluation/)
- [Heuristic Evaluations and Expert Reviews - usability.gov](http://www.usability.gov/how-to-and-tools/methods/heuristic-evaluation.html)
- [Prescriptive Theories - cs.umd.edu](http://www.cs.umd.edu/class/fall2002/cmsc838s/tichi/prescriptive.html)


### 8.1.4 Going further

Yes, there are more rules. As usual, this only covers a small part of what one should know. Of honorable mention, there is Toggazini's list of interface rules that you should also read. His set of rules confirm what has been previously said, but the author goes deeper and suggests solutions on how to accomplish some results (like having fast feedback by using multi-threadted apps) and adds in a few of his own. However, since this article already has enough rules as it is, we simply provide with links that you can explore:

- [First Principles of Interaction Design - asktog.com](http://www.asktog.com/basics/firstPrinciples.html)
- [Theories, Principles, and Guidelines - ppt - UTPA Faculty Web](http://tinyurl.com/ot5bxv8)

The content of this subsection was based on slides [IPM6-DesignRules-InfoVisual-colors-GraphicDesign11-12-en-clip][IPM-6]. These set of slides covers all the rules we introduted you to in sub sections 8.1.1-8.1.4 and it also mentions other very important aspects of design such as visual collection and interpretation of information, color interaction, legibility, graphic design and typography. This information is sure to help those of you going to for  overall graphic design. 

We also covered some information from the [IPM8-GOMS-Evaluation-11-12-en-clip][IPM-8] set of slides by going through Nielsen's heuristics. However, we left out other important information such as predictive models (which predict how people interact with interfaces) and a comparision between the various used models, interface evaluation (we covered it a little bit with Nielsen) and user testing, which is a very rich a long chapter not covered here that presents you with several ways of testing your interface with users in order to make sure you are doing a good job.



----------------------

**Evaluate your design** 
when you have your chosen design ready you have a few more things to consider.
have a friend or family member help you answer these questions.

 - can users easily find what they are looking for?
 - how long will it take the average user to skim over the websites contents?


![imgfind][17]

--------------

 - is it hard to read text?
 - is the site to cluttered?


 ![imgclutter][18].

 


### 8.2 Specific website design rules

- Hack design
- [6 Tips for a Great Flex UX: Part 5](http://designingwebinterfaces.com/6-tips-for-a-great-flex-ux-part-5)

### Golden rules of Web design:
 - http://www.smashingmagazine.com/2008/05/29/applying-divine-proportion-to-web-design/
 - http://web.clubrunner.ca/inner-lang-1-1.aspx?pageid=204
 - http://positionfrontpage.com/seo-articles/Web_Design_Simple_Mistakes_And_Golden_Rules.php
 - http://ec.europa.eu/ipg/standards/accessibility/10_rules/
 - http://www.webdesignfromscratch.com/basics/golden-rule/


 ------------

More useful resources 
-------
An interesting video on the subject that you should check out:
[The Art of Web Design | Off Book | PBS Digital Studios](http://www.youtube.com/watch?v=3iVVM_DgWY4)

Helpful font and tools
[Check out our Fonts and Color pickers/pallets](http://www.codecademy.com/groups/html-projects/discussions/52162934f10c60c4da003760)

Learn how to create a vintage type effects
[Create a Vintage Type Effect](http://www.codecademy.com/groups/html-projects/discussions/523b879b548c35f69d00013a)


When you're ready to actually build your website:
 [What do I need to learn how to build a website?](http://www.codecademy.com/groups/html-projects/discussions/51e69fe27c82ca29510040c3).

If you would like to try your hand at designing a webpage, check out one of our [Web Development Challenges](http://www.codecademy.com/groups/html-projects/discussions/51c0bb0d7c82cafb9d0002a9)

**Q:** i dont want to pay for web hosting just to practice web design:
**A:** For free web hosting you should check out our [guide](http://www.codecademy.com/groups/html-projects/discussions/5167394881c2702060000d1f), just in case you want to actually practice making one of your mock ups for the web

Left out:


- [IPM5-Interface-Analisys-Conceptual-Models11-12-en][IPM-5]
- [IPM7-Icon-Dialog-Implementation11-12-en][IPM-7]
- [IPM9-InteractionMachine11-12-en-clip][IPM-9]
- [IPM10-Paradigms-FuturePersp-links11-12-en][IPM-10]